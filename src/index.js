const LClient = require('./Structures/LClient');
const config = require('../config.json')
const Discord = require('discord.js')
const mongo = require('../src/mongo')

const client = new LClient(config);

client.start();

const connectToMongoDB = async () => {
    await mongo().then((mongoose) => {
        try {
            console.log(`Connected to mongodb!`)
        } finally {
            mongoose.connection.close()
        }
    })
}

connectToMongoDB()