const { MessageEmbed, Discord } = require('discord.js')
const Command = require('../Structures/Command');
const { lunercolour } = require('../../config.json')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Simple hello command'
        })
    }

    async run(message) {
        const embed = new MessageEmbed()
            .setDescription(`👋 Hello <@!${message.author.id}>`)
            .setTimestamp()
            .setColor(lunercolour)
            .setFooter(`Requested By: ${message.author.username}`)
        message.channel.send(embed)

    }
}