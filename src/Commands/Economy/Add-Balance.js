const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const economy = require('../../Economy');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Add a balance to yourself or someone else',
            category: 'Economy',
            usage: '<@User> <Amount to add>',
            aliases: ['add-money', 'add-bal']
        })
    }

    async run(message, ...args) {

        if (!message.member.hasPermission('MANAGE_GUILD')) return
        if (message.channel.type === 'DM') return
        if (message.member.bot) return

        const mention = message.mentions.users.first() || message.author

        const coins = args[1]
        if (isNaN(coins)) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Add-Balance`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Add a balance to yourself or someone else`)
                    .addField(`Usage:`, `${this.client.prefix}add-balance <@User> <Amount to add>`)
                    .addField(`Example`, `${this.client.prefix}add-balance @James 500`)
            )
        }

        const guildId = message.guild.id
        const userId = mention.id

        const newCoins = await economy.addCoins(guildId, userId, coins)

        message.channel.send(
            new Discord.MessageEmbed()
            .setTitle(`Money Added!`)
            .setColor(lunercolour)
            .setDescription(`Added **${coins}** to <@!${userId}>'s Balance`)
            .setTimestamp()
            .setFooter(`Money Added By: ${message.author.username}`)
        )
    }
}