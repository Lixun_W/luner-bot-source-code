const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const economy = require('../../Economy');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Check how much money you have',
            category: 'Economy',
            usage: '[@User]',
            aliases: ['bal', 'money', 'money-check']
        })
    }

    async run(message) {

        const target = message.mentions.users.first() || message.author

        if (message.channel.type === 'DM') return
        if (message.member.bot) return
    
        const guildId = message.guild.id
        const userId = target.id
    
        const coins = await economy.getCoins(guildId, userId)

        message.channel.send(
            new Discord.MessageEmbed()
            .setTitle(`Balance`)
            .setColor(lunercolour)
            .setDescription(`<@!${userId}>'s Balance Is: **${coins}**`)
            .setTimestamp()
            .setFooter(`Requested By: ${message.author.username}`)
        )
    }
}