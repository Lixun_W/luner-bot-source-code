const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const economy = require('../../Economy');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Give someone money',
            category: 'Economy',
            usage: '<@User> <Amount to give>',
            aliases: ['give-money']
        })
    }

    async run(message, ...args) {

        const { guild, member } = message

        const target = message.mentions.users.first()
        if (!target) {
            message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Pay`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Give someone money`)
                    .addField(`Usage:`, `${this.client.prefix}pay <@User> <Amount to give>`)
                    .addField(`Example`, `${this.client.prefix}pay @James 500`)
            )
            return
        }

        const coinsToGive = args[1]
        if (isNaN(coinsToGive)) {
            message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Pay`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Give someone money`)
                    .addField(`Usage:`, `${this.client.prefix}pay <@User> <Amount to give>`)
                    .addField(`Example`, `${this.client.prefix}pay @James 500`)
            )
            return
        }

        const coinsOwned = await economy.getCoins(guild.id, member.id)
        if (coinsOwned < coinsToGive) {
            message.channel.send(
                new Discord.MessageEmbed()
                .setTitle(`Insufficient Funds`)
                .setColor(lunercolour)
                .setDescription(`You do not have the amount of money you are trying to give`)
                .setTimestamp()
                .setFooter(`Insufficient Funds`)
            )
            return
        }

        const remainingCoins = await economy.addCoins(
            guild.id,
            member.id,
            coinsToGive * -1
        )
        const newBalance = await economy.addCoins(guild.id, target.id, coinsToGive)

        message.channel.send(
            new Discord.MessageEmbed()
            .setTitle(`Money Given`)
            .setColor(lunercolour)
            .setDescription(`You have given ${coinsToGive} to <@!${target.id}>`)
            .setTimestamp()
            .setFooter(`Money Given By: ${message.author.username}`)
        )
    }
}