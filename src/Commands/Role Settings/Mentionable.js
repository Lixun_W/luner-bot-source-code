const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const { suppressDeprecationWarnings } = require('moment');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Set a role mentionable or not',
            category: 'Role Settings',
            usage: '<Yes / No> <@Role>'
        })
    }

    async run(message) {

        const role = message.mentions.roles.first()

        if (!message.member.hasPermission('MANAGE_ROLES')) return
        if (message.channel.type === 'DM') return
        if (message.member.bot) return

        if (!role) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Mentionable`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Toggle a role to be mentionable or not`)
                    .addField(`Usage:`, `${this.client.prefix}mentionable <Yes / No> @role`)
                    .addField(`Example`, `${this.client.prefix}mentionable Yes @Owner`)
            )
        }

        if (message.content.includes('yes')) {
            role.edit({ mentionable: true })
        } else if (message.content.includes('Yes')) {
            role.edit({ mentionable: true })
        } else if (message.content.includes('No')) {
            role.edit({ mentionable: false })
        } else if (message.content.includes('no')) {
            role.edit({ mentionable: false })
        }

        message.channel.send(
            new Discord.MessageEmbed()
            .setColor(lunercolour)
            .setDescription(`Changed the mentionability of <@&${role.id}>`)
        )
    }
}