const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Set the name of the guild',
            category: 'Server Settings',
            usage: '<New Name>'
        });
    }

    async run(message, ...args) {

        if (!message.member.hasPermission('MANAGE_GUILD')) return
        if (message.channel.type === 'DM' || message.member.bot) return

        const NewName = args.join(' ')

        if (!NewName) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Guild-name`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Set the name of the guild`)
                    .addField(`Usage:`, `${this.client.prefix}guild-name <New Name>`)
                    .addField(`Example:`, `${this.client.prefix}guild-name Luner Support Server`)
            )
        }

        message.guild.edit({
            name: `${NewName}`
        })

        message.channel.send(
            new Discord.MessageEmbed()
                .setTitle(`Name Set!`)
                .setDescription(`The guild name has now been set to: ${NewName}`)
                .setColor(lunercolour)
                .setTimestamp()
                .setFooter(`Name changed by: ${message.author.username}`)
        )
    }

}