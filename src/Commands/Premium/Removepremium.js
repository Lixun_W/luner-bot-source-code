const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const premiumuserSchema = require('../../Schemas/premiumuser-schema');
const { lunercolour } = require('../../../config.json');
const mongo = require('../../mongo');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Remove someone from the premium command list',
            category: 'Premium',
            usage: '<Guild ID>'
        })
    }

    async run(message, ...args) {

        if (message.author.id != '438756132937269248') return

        const guildId = message.guild.id

        await mongo().then(async (mongoose) => {
            try {
                await premiumuserSchema.deleteOne(
                    {
                        _id: guildId,
                    },


                )

                message.channel.send(
                    new Discord.MessageEmbed()
                        .setTitle(`Premium Removed!`)
                        .setColor(lunercolour)
                        .setDescription(`${message.guild.name} Is now off the premium list!`)
                        .setThumbnail(message.guild.iconURL())
                        .setTimestamp()
                        .setFooter(`Premium Removed!`)
                )

            } finally {
                mongoose.connection.close()
            }
        })
    }
}