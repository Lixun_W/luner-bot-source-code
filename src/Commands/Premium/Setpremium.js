const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const premiumuserSchema = require('../../Schemas/premiumuser-schema');
const { lunercolour } = require('../../../config.json');
const mongo = require('../../mongo');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Set a guild to premium which give them the access to premium commands',
            category: 'Premium',
            usage: '<Guild ID>'
        })
    }

    async run(message, ...args) {

        if (message.author.id != '438756132937269248') return

        const guildId = message.guild.id

        await mongo().then(async (mongoose) => {
            try {
                await premiumuserSchema.findOneAndUpdate(
                    {
                        _id: guildId,
                    },
                    {
                        _id: guildId,
                        guildId: guildId,
                    },
                    {
                        upsert: true,
                    }
                )

                message.channel.send(
                    new Discord.MessageEmbed()
                        .setTitle(`Premium Set!`)
                        .setColor(lunercolour)
                        .setDescription(`${message.guild.name} Is now on the premium list!`)
                        .setThumbnail(message.guild.iconURL())
                        .setTimestamp()
                        .setFooter(`Premium Set!`)
                )

            } finally {
                mongoose.connection.close()
            }
        })
    }
}