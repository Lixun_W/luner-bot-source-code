const { MessageEmbed, Discord } = require('discord.js');
const { lunercolour } = require('../../../config.json');
const Command = require('../../Structures/Command');

module.exports = class extends Command {
    constructor(...args) {
        super(...args, {
            description: 'Shows a users avatar.',
            aliases: ['icon', 'pfp', 'pic'],
            category: 'Misc',
            usage: '[@user]'
        })
    }
    async run(message) {

        const user = message.mentions.members.last() || message.member;
        const AvatarEmbed = new MessageEmbed()
            .setDescription(`${user.user.username}'s Avatar`)
            .setColor(lunercolour)
            .setImage(user.user.displayAvatarURL({ dynamic: true, size: 512 }))
            .setTimestamp()
            .setFooter(`Requested By: ${message.author.username}`);
        message.channel.send(AvatarEmbed)
    }
}