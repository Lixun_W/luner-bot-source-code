const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Get the link to invite luner to your server',
            category: 'Misc',
        })
    }

    async run(message) {

        message.channel.send(
            new Discord.MessageEmbed()
            .setTitle(`Luner Bot Invite`)
            .setColor(lunercolour)
            .setDescription(`[Luner Invite](https://discord.com/api/oauth2/authorize?client_id=761557383654539285&permissions=8&scope=bot)`)
            .addField(`Thank you so Much!`, `Thank you for adding Luner it helps me and my development team get one step closer to verification!`)
            .setTimestamp()
            .setFooter(`Requested By: ${message.author.username}`)
        )
    }
}