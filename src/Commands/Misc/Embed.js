const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Repeat something in an embed',
            category: 'Misc',
            usage: '<Message>'
        })
    }

    async run(message) {

        const EmbedMessage = message.content.substring(6)

        if (!message.member.hasPermission('MANAGE_MESSAGES')) return
        if (message.channel.type === 'DM') return
        if (message.member.bot) return

        if (!EmbedMessage) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Embed`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Embed a message that was repeated`)
                    .addField(`Usage:`, `${this.client.prefix}embed <Message>`)
                    .addField(`Example`, `${this.client.prefix}embed Hello!`)
            )
        }

        message.delete()
        message.channel.send(
            new Discord.MessageEmbed()
            .setTitle(`New Message!`)
            .setColor(lunercolour)
            .setDescription(EmbedMessage)
            .setTimestamp()
            .setFooter(`Said by: ${message.author.username}`)
        )
    }
}