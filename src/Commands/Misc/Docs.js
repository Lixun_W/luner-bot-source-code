const fetch = require("node-fetch")
const Discord = require("discord.js")
const Command = require("../../Structures/Command")
const { lunercolour } = require('../../../config.json')

module.exports = class extends Command {
    constructor(...args) {
        super(...args, {
            description: 'Shows the discord.js Docs',
            aliases: [],
            category: 'Misc',
            usage: '<query>'
        })
    }
    async run(message) {

        let msgArray = message.content.split(" ");
        let args = msgArray.slice(1);
        message.delete();
        let query = args.join(' ');
        let djsurl = `https://djsdocs.sorta.moe/v2/embed?src=stable&q=${encodeURIComponent(query)}`;

        if (!query) {
            const rEmbed = new Discord.MessageEmbed()
                .setColor(lunercolour)
                .setTitle("Missing Arguments")
                .setDescription("Please provide a query to search. `;docs <query>`");

            return message.channel.send(rEmbed);
        }
        fetch(djsurl)
            .then(res => res.json())
            .then(embed => {
                if (embed && !embed.error) {
                    message.channel.send({ embed });
                } else {
                    const fEmbed = new Discord.MessageEmbed()
                        .setColor(lunercolour)
                        .setTitle("Error")
                        .setDescription(`Sorry, but \`${query}\` isn't in the discord.js-stable documentation.`);

                    message.channel.send(fEmbed);
                }
            })

            .catch(e => {
                console.error(e);
                const dEmbed = new Discord.MessageEmbed()
                    .setColor(lunercolour)
                    .setTitle("Error")
                    .setDescription("Something went wrong. Please try again!");

                message.channel.send(dEmbed);
            })
    }
}