const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Get the bot to repeat something',
            category: 'Misc',
            usage: '<Message> [Embed]'
        })
    }

    async run(message) {

        let msgArray = message.content.split(" ");
        let args = msgArray.slice(1);
        let content = args.join(' ') 

            message.channel.send(`${content}\n- <@${message.author.id}>`)
    }
}