const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Load A company server template into your server',
            category: 'Server Templates',
        });
    }

    async run(message) {

        if (!message.member.hasPermission({ checkOwner: true })) return
        if (message.channel.type === 'DM' || message.member.bot) return

        message.guild.channels.cache.forEach((channel) => channel.delete())
        message.guild.channels.cache.forEach((roles) => roles.delete())

        const Guild = this.client.guilds.cache.get('779113559992827965')
        message.guild.channels.cache.filter(ch => ch.type === 'category').forEach(parent => {
            Guild.channels.create(parent.name, {
                type: 'category',
                topic: `Copied category from ${message.guild.name}`,
                permissionOverwrites: [{
                    id: Guild.id,
                    allow: parent.permissionOverwrites.get(message.guild.id).allow,
                    deny: parent.permissionOverwrites.get(message.guild.id).deny
                }]
            }).then(category => {
                parent.children.forEach(channel => {
                    Guild.channels.create(channel.name, {
                        type: channel.type,
                        parent: category,
                        permissionOverwrites: [{
                            id: Guild.id,
                            allow: channel.permissionOverwrites.get(message.guild.id).allow,
                            deny: channel.permissionOverwrites.get(message.guild.id).deny
                        }]
                    })
                })
            }).catch(err => {
                message.channel.send(
                    new Discord.MessageEmbed()
                        .setTitle(`Stand By!`)
                        .setColor(lunercolour)
                        .setDescription(`Please standby while I am fixing permission!`)
                        .setTimestamp()
                        .setFooter(`Permissions Being Fixed`)
                )
            })
        })

        message.guild.roles.cache.forEach(role => {
            Guild.roles.create({
                data: {
                    name: role.name,
                    color: role.color,
                    permissions: role.permissions,
                    mentionable: role.mentionable,
                    position: role.position
                }
            })
        })
    }
}