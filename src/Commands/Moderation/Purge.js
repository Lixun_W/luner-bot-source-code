const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Get rid of a number of messages',
            category: 'Moderation',
            usage: '<Number of Messages>'
        })
    }

    async run(message) {

        const args = message.content.split(' ').slice(1);
        let amount = args.join(' ')

        if (!message.member.hasPermission('MANAGE_MESSAGES')) return
        if (message.member.bot) return
        if (message.channel.type === 'DM') return

        if (!amount) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Purge`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Mass delete messages from the guild`)
                    .addField(`Usage:`, `${this.client.prefix}purge <Number Of Messages>`)
                    .addField(`Example`, `${this.client.prefix}purge 100`)
            )
        }

        if (isNaN(amount)) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Purge`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Mass delete messages from the guild`)
                    .addField(`Usage:`, `${this.client.prefix}purge <Number Of Messages>`)
                    .addField(`Example`, `${this.client.prefix}purge 100`)
            )
        }

        if (amount > 100) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Purge`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Mass delete messages from the guild`)
                    .addField(`Usage:`, `${this.client.prefix}purge <Number Of Messages>`)
                    .addField(`Example`, `${this.client.prefix}purge 100`)
            )
        }

        if (amount < 1) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Purge`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Mass delete messages from the guild`)
                    .addField(`Usage:`, `${this.client.prefix}purge <Number Of Messages>`)
                    .addField(`Example`, `${this.client.prefix}purge 100`)
            )
        }
        try {
            const fetched = await message.channel.messages.fetch({ limit: 100 });
            const notPinned = fetched.filter(fetchedMsg => !fetchedMsg.pinned);
          
            await message.channel.bulkDelete(notPinned, true);
          } catch(err) {
            console.error(err);
          }
    }
}