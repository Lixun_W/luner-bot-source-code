const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const ms = require('ms')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Add a timer before people can type in a channel again',
            category: 'Moderation',
            usage: '<Time>'
        })
    }

    async run(message) {

        let args = message.content.slice(this.client.prefix.length).trim().split(/\s+/g);
        let channel = message.mentions.channels.first(),
        time = args[2];

        if (!channel) time = args[1], channel = message.channel;

        if (!message.member.hasPermission('MANAGE_CHANNELS')) return
        if (message.member.bot) return
        if (message.channel.type === 'DM') return

        if (!time) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Slowmode`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Add a timer before someone can type again. Max - 6H Min - 1S`)
                    .addField(`Usage:`, `${this.client.prefix}slowmode <Time>`)
                    .addField(`Example`, `${this.client.prefix}slowmode 10s`)
            )
        }

        if (args[0].toLowerCase() === 'off' || args[1].toLowerCase() === 'off') {
            channel.setRateLimitPerUser(0)
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setDescription(`Slowmode set to off`)
                    .setColor(lunercolour)
            )
        }

        const convert = ms(time)
        const toSecond = Math.floor(convert / 1000);

        if (!toSecond || toSecond == undefined) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Slowmode`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Add a timer before someone can type again. Max - 6H Min - 1S`)
                    .addField(`Usage:`, `${this.client.prefix}slowmode <Time>`)
                    .addField(`Example`, `${this.client.prefix}slowmode 10s`)
            )
        }

        if (toSecond > 21600) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Slowmode`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Add a timer before someone can type again. Max - 6H Min - 1S`)
                    .addField(`Usage:`, `${this.client.prefix}slowmode <Time>`)
                    .addField(`Example`, `${this.client.prefix}slowmode 10s`)
            )
        } else if (toSecond < 1) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Slowmode`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Add a timer before someone can type again. Max - 6H Min - 1S`)
                    .addField(`Usage:`, `${this.client.prefix}slowmode <Time>`)
                    .addField(`Example`, `${this.client.prefix}slowmode 10s`)
            )
        }

        await channel.setRateLimitPerUser(toSecond)
            message.channel.send(
                new Discord.MessageEmbed()
                    .setDescription(`Set slowmode to: ${time}`)
                    .setColor(lunercolour)
            )
        .catch((e) => console.log(e))
    }
}