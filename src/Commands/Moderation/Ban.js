const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const mongo = require('../../mongo');
const banSchema = require('../../Schemas/ban-schema');
const modlogSchema = require('../../Schemas/modlog-schema');

module.exports = class extends Command {

  constructor(...args) {
    super(...args, {
      description: 'Ban someone from your guild',
      category: 'Moderation',
      aliases: ['b'],
      usage: '<@User> [Reason]',
      userPerms: 'BAN_MEMBERS'
    })
  }

  async run(message) {

    const mentionMember = message.mentions.members.first()

    let msgArray = message.content.split(" ");
    let args = msgArray.slice(2);
    let reason = args.join(' ') || 'No reason Provided'


    if (!message.member.hasPermission('BAN_MEMBERS')) return

    if (message.channel.type === 'DM') return

    if (!mentionMember) {
      return message.channel.send(
        new Discord.MessageEmbed()
          .setTitle(`Command: Ban`)
          .setColor(lunercolour)
          .addField(`Description:`, `Ban someone from the guild`)
          .addField(`Usage:`, `${this.client.prefix}ban <@User> [Reason]`)
          .addField(`Example`, `${this.client.prefix}ban @Lukas Goodbye!`)
      )

    }

    try {
      mentionMember.send(`You have been banned from \`${message.guild.name}\` with the reason of: ${reason}`)
    } catch (error) {
      message.channel.send(`Could not send DM to this user`)
    }

    mentionMember.ban()
      .then(() => {
        message.channel.send(
          new Discord.MessageEmbed()
            .setDescription(`${mentionMember.user.username}#${mentionMember.user.discriminator} was banned || Reason: ${reason}`)
            .setColor(lunercolour)
        )
      })

    var puncode = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < 10; i++) {
      puncode += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    const guildId = message.guild.id
    const userId = mentionMember.id
    const type = 'Ban'
    const user = mentionMember.user.username

    const bans = {
      author: message.member.user.tag,
      timestamp: new Date().getTime(),
      reason,
      puncode,
    }

    const modlog = {
      author: message.member.user.tag,
      timestamp: new Date().getTime(),
      reason,
      puncode,
      type,
      user,
    }

    await mongo().then(async (mongoose) => {
      try {
        await banSchema.findOneAndUpdate(
          {
            guildId,
            userId,
          },
          {
            guildId,
            userId,
            $push: {
              bans: bans,
            },
          },
          {
            upsert: true,
          }
        )

        await modlogSchema.findOneAndUpdate(
          {
            guildId,
          },
          {
            guildId,
            $push: {
              modlogs: modlog,
            },
          },
          {
            upsert: true,
          }
        )
      } finally {
        mongoose.connection.close()
      }
    })
  }
}