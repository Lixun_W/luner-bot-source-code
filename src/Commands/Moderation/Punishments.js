const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const mongo = require('../../mongo');
const warnSchema = require('../../Schemas/warn-schema');
const { lunercolour } = require('../../../config.json');
const banSchema = require('../../Schemas/ban-schema');
const kickSchema = require('../../Schemas/kick-schema');

module.exports = class extends Command {

  constructor(...args) {
    super(...args, {
      description: 'List the punishments of someone',
      category: 'Moderation',
      aliases: ['List-punishments'],
      usage: '[@user]'
    })
  }

  async run(message) {

    const member = message.mentions.members.first() || message.author

    const guildId = message.guild.id
    const userId = member.id

    await mongo().then(async (mongoose) => {
      try {
        const results = await warnSchema.findOne({
          guildId,
          userId,
        })

        const banResults = await banSchema.findOne({
          guildId,
          userId,
        })

        const kickResults = await kickSchema.findOne({
          guildId,
          userId,
        })

        const embed = new Discord.MessageEmbed()

        if (!banResults) {
          embed.setColor(lunercolour)
        } else {
          for (const ban of banResults.bans) {
            if (!banResults) return embed.setDescription(`No punishments`)
            const { author, timestamp, reason, puncode } = ban
            embed.addField(`Ban:`, `Author - ${author}\nDate - ${new Date(timestamp).toLocaleDateString()}\nReason - ${reason}\nInfraction ID: ${puncode}`)
          }
        }

        if (!kickResults) {
          embed.setColor(lunercolour)
        } else {
          for (const kick of kickResults.kicks) {
            if (!kickResults) return embed.setDescription(`No punishments`)
            const { author, timestamp, reason, puncode } = kick
            embed.addField(`Kick:`, `Author - ${author}\nDate - ${new Date(timestamp).toLocaleDateString()}\nReason - ${reason}\nInfraction ID: ${puncode}`)
          }
        }

        if (!results) {
          embed.setColor(lunercolour)
        } else {
          for (const warning of results.warnings) {
            const { reason, puncode } = warning
            embed.addField(`Case #${puncode} - warn`, `${reason}`)
          }
        }

        embed.setAuthor(member.user.tag, member.user.displayAvatarURL({ dynamic: true }))
        embed.setColor('#04acf4')

        message.channel.send(embed)
      } finally {
        mongoose.connection.close()
      }
    })
  }
}