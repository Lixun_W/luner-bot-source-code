const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const mongo = require('../../mongo');
const kickSchema = require('../../Schemas/kick-schema')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Kick someone from your guild',
            category: 'Moderation',
            aliases: ['k'],
            usage: '<@User> [Reason]'
        })
    }

    async run(message) {

        let msgArray = message.content.split(" ");
        let args = msgArray.slice(2);
        let reason = args.join(' ') || 'No reason provided'

        const mentionMember = message.mentions.members.first()

        if (!message.member.hasPermission('KICK_MEMBERS')) return

        if (message.channel.type === 'DM') return

        if (!mentionMember) {
            message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Kick`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Kick someone from the guild`)
                    .addField(`Usage:`, `${this.client.prefix}kick <@User> [Reason]`)
                    .addField(`Example`, `${this.client.prefix}kick @Lukas Goodbye!`)
            )
            return
        }

        if (!mentionMember.kickable) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setDescription(`I can't kick ${mentionMember.username}#${mentionMember.discriminator}`)
                    .setColor(lunercolour)
            )
        }

        try {
            mentionMember.send(`You have been kicked from \`${message.guild}\` with the reason of: ${reason}`)
        } catch (error) {
            message.channel.send(`Could not send DM to this user`)
        }

        mentionMember.kick()
            .then(() => {
                message.channel.send(
                    new Discord.MessageEmbed()
                        .setDescription(`${mentionMember.user.username}#${mentionMember.user.discriminator} was kicked || Reason: ${reason}`)
                        .setColor(lunercolour)
                )
            })

        var puncode = "";
        var characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var charactersLength = characters.length;
        for (var i = 0; i < 10; i++) {
            puncode += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        const guildId = message.guild.id
        const userId = mentionMember.id
        const type = 'Kick'
        const user = mentionMember.user.username

        const kicks = {
            author: message.member.user.tag,
            timestamp: new Date().getTime(),
            reason,
            puncode,
        }

        const modlog = {
            author: message.member.user.tag,
            timestamp: new Date().getTime(),
            reason,
            puncode,
            type,
            user,
          }

        await mongo().then(async (mongoose) => {
            try {
                await kickSchema.findOneAndUpdate(
                    {
                        guildId,
                        userId,
                    },
                    {
                        guildId,
                        userId,
                        $push: {
                            kicks: kicks,
                        },
                    },
                    {
                        upsert: true,
                    }
                )

                await modlogSchema.findOneAndUpdate(
                    {
                      guildId,
                    },
                    {
                      guildId,
                      $push: {
                        modlogs: modlog,
                      },
                    },
                    {
                      upsert: true,
                    }
                  )
            } finally {
                mongoose.connection.close()
            }
        })
    }
}