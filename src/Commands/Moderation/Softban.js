const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const modlogSchema = require('../../Schemas/modlog-schema');
const banSchema = require('../../Schemas/ban-schema');
const mongo = require('../../mongo')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Bans and unbans someone from the guild to get rid of their messages',
            category: 'Moderation',
            usage: '<@User> [Reason]',
            aliases: ['sb']
        });
    }

    async run(message) {

        if (!message.member.hasPermission('BAN_MEMBERS')) return;
        if (message.channel.type === 'DM') return;
        if (message.member.bot) return;

        let msgArray = message.content.split(" ");
        let args = msgArray.slice(2)
        let reason = args.join(' ') || 'No Reason Provided'

        const member = message.mentions.members.first()

        if (!member) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Softban`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Bans and unbans someone from the guild to get rid of their messages`)
                    .addField(`Usage:`, `${this.client.prefix}softban <@User> [Reason]`)
                    .addField(`Example:`, `${this.client.prefix}softban @Lukas Goodbye!`)
            )
        }

        if (!member.bannable) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setDescription(`I can't kick ${member.tag}`)
                    .setColor(lunercolour)
            )
        }

        try {
            member.send(`You have been softbanned from \`${message.guild.name}\` with the reason of: ${reason}`)
        } catch (error) {
            message.channel.send(`Could not send DM to this user`)
        }

        member.ban()

        message.guild.members.unban(member.id)

        const puncode = "";
        const characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        const charactersLength = characters.length;
        for (var i = 0; i < 10; i++) {
            puncode += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        const guildId = message.guild.id
        const userId = member.id
        const type = 'SoftBan'
        const user = member.user.username

        const bans = {
            author: message.member.user.tag,
            timestamp: new Date().getTime(),
            reason,
            puncode,
        }

        const modlog = {
            author: message.member.user.tag,
            timestamp: new Date().getTime(),
            reason,
            puncode,
            type,
            user,
        }

        await mongo().then(async (mongoose) => {
            try {
                await banSchema.findOneAndUpdate(
                    {
                        guildId,
                        userId,
                    },
                    {
                        guildId,
                        userId,
                        $push: {
                            bans: bans,
                        },
                    },
                    {
                        upsert: true
                    }
                )

                await modlogSchema.findOneAndUpdate(
                    {
                        guildId,
                    },
                    {
                        guildId,
                        $push: {
                            modlogs: modlog,
                        },
                    },
                    {
                        upsert: true,
                    }
                )
            } finally {
                mongoose.connection.close()
            }
        })
    }
}