const mongo = require('../../mongo');
const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const warnSchema = require('../../Schemas/warn-schema');
const { lunercolour } = require('../../../config.json');
const modlogSchema = require('../../Schemas/modlog-schema');

module.exports = class extends Command {

  constructor(...args) {
    super(...args, {
      description: 'Warn someone',
      category: 'Moderation',
      usage: '<@user> [Reason]'
    })
  }
  
  async run(message) {

    const mentionMember = message.mentions.members.first()

    if (!message.member.hasPermission('MANAGE_MESSAGES')) return

    if (message.channel.type === 'DM') return
    
    if (!mentionMember) {
      return message.channel.send(
        new Discord.MessageEmbed()
        .setAuthor(message.author.tag, message.author.displayAvatarURL({ dynamic: true }))
        .setDescription(`<:xmark:805522746955137064> Too few arguments given.\n\nUsage:\n\`warn <member> <reason>\``)
        .setColor('#ec5454')
      )
    }

    if (message.author.id === mentionMember.id) {

      message.channel.send(
        new Discord.MessageEmbed()
        .setAuthor(message.author.tag, message.author.displayAvatarURL({ dynamic: true }))
        .setDescription(`<:xmark:805522746955137064> You cannot warn yourself!`)
        .setColor('#ec5454')
      )
      return
    }

    var puncode = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < 10; i++) {
      puncode += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    let msgArray = message.content.split(" ");
    let args = msgArray.slice(2);
    let reason = args.join(' ') || 'No Reason Provided'

    const embed = new Discord.MessageEmbed()
    .setColor(lunercolour)
    .setDescription(`**Server:** ${message.guild.name}\n**Warned By:** <@!${message.author.id}>\n**Action:** Warn\n**Reason:** ${reason}\n`)
    .setFooter(`Unique Warn ID: ${puncode}`)
    .setTimestamp()

    try { 
      mentionMember.send(embed)
    } catch(e) {
      console.log(`Unable to DM Member`)
    }

    const guildId = message.guild.id
    const userId = mentionMember.id
    const type = 'Warn'
    const user = mentionMember.user.username
    const code = puncode

    const warning = {
      author: message.member.user.tag,
      timestamp: new Date().getTime(),
      reason,
      puncode
    }

    const modlog = {
      author: message.member.user.tag,
      timestamp: new Date().getTime(),
      reason,
      puncode,
      type,
      user,
    }

    await mongo().then(async (mongoose) => {
      try {

        await warnSchema.findOneAndUpdate(
          {
            guildId,
            userId,
            code,
          },
          {
            guildId,
            userId,
            code,
            $push: {
              warnings: warning,
            },
          },
          {
            upsert: true,
          }
        )

      await modlogSchema.findOneAndUpdate(
        {
          guildId,
        },
        {
          guildId,
          $push: {
            modlogs: modlog,
          },
        },
        {
          upsert: true,
        }
      )
      } finally {
        mongoose.connection.close()
      }
    })

    message.channel.send(`<:check:808087593388081162> \`Case #${code}\` <@${mentionMember.id}> has been warned.`)
  }
}