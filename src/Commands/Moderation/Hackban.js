const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const mongo = require('../../mongo');
const banSchema = require('../../Schemas/ban-schema');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Ban someone who isn\'t in the server',
            category: 'Moderation',
            usage: '<ID> [Reason]'
        });
    }

    async run(message, ...args) {

        const id = args[0]

        if (!message.member.hasPermission('BAN_MEMBERS')) return;

        if (!id)
        {
            message.channel.send(
                new Discord.MessageEmbed()
                    .setAuthor(message.author.tag, message.author.displayAvatarURL({ dynamic: true }))
                    .setDescription(`Please input an ID To ban someone`)
                    .setTimestamp()
                    .setColor('DARK_RED')
            )
            return
        }
    }
}