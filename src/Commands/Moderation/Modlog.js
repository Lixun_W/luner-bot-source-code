const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const mongo = require('../../mongo');
const modlogSchema = require('../../Schemas/modlog-schema')
const { lunercolour } = require('../../../config.json')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Show all of the moderation that has happened within the server',
            category: 'Moderation',
            aliases: ['List-moderation', 'modlogs'],
        })
    }

    async run(message) {

        if (!message.member.hasPermission('MANAGE_GUILD')) return
        if (message.channel.type === 'DM') return
        if (message.member.bot) return
        const guildId = message.guild.id

        await mongo().then(async (mongoose) => {
            try {
                const results = await modlogSchema.findOne({
                    guildId,
                })

                const embed = new Discord.MessageEmbed()
                if (!results) {
                    embed.setDescription(`There is no record of a modlog for this server`)
                } else {
                    for (const modlog of results.modlogs) {
                        const { author, timestamp, reason, puncode, type, user } = modlog
                        embed.addField(`${type}:`, `Author - ${author}\nDate - ${new Date(timestamp).toLocaleDateString()}\nUser: ${user}\nReason - ${reason}\nInfraction ID: ${puncode}`)
                    }
                }

                embed.setTitle(`Modlog For ${message.guild.name}`)
                embed.setTimestamp()
                embed.setColor(lunercolour)
                embed.setFooter(`Requested By: ${message.author.username}`)

                message.channel.send(embed)
            } finally {
                mongoose.connection.close()
            }
        })
    }
}