const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const warnSchema = require('../../Schemas/warn-schema')
const { lunercolour } = require('../../../config.json')
const mongo = require('../../mongo')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Clear a warn from someone',
            category: 'Moderation',
            usage: '<@user>'
        })
    }

    async run(message, ...args) {

        if (!message.member.hasPermission('KICK_MEMBERS')) return

        if (!args[0]) {
            message.channel.send(
                new Discord.MessageEmbed()
                    .setAuthor(message.author.tag, message.author.displayAvatarURL({ dynamic: true }))
                    .setDescription(`<:xmark:805522746955137064> Invalid \`<case ID>\` argument given\n\nUsage:\n\`remove-case <case ID>\``)
                    .setColor("#ec5454")
            )
            return
        }

        const guildId = message.guild.id
        const code = args[0]

        await mongo().then(async (mongoose) => {
            try {
                const results = await warnSchema.findOneAndDelete({
                    guildId,
                    code,
                })

                if (!results) {
                    message.channel.send(
                        new Discord.MessageEmbed()
                            .setAuthor(message.author.tag, message.author.displayAvatarURL({ dynamic: true }))
                            .setDescription(`<:xmark:805522746955137064> Invalid \`<case ID>\` argument given\n\nUsage: \`remove-case <case ID>\``)
                            .setColor("#ec5454")
                    )
                    return
                }
                if (results) {
                    message.channel.send(
                        new Discord.MessageEmbed()
                            .setDescription(`Warn cleared`)
                            .setColor(lunercolour)
                    )
                }

            } finally {
                mongoose.connection.close()
            }
        })


    }
}