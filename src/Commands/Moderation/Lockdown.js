const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Lockdown a channel to stop people from typing',
            category: 'Moderation',
        })
    }

    async run(message) {

        const { channel } = message

        if (!message.member.hasPermission('MANAGE_CHANNELS')) return
        if (message.member.bot) return
        if (message.channel.type === 'DM') return

        if (
            channel.permissionsFor(message.guild.id).has('SEND_MESSAGES') === false
        ) {
            channel.updateOverwrite(message.guild.id, {
                SEND_MESSAGES: true,
            })

            message.channel.send(
                new Discord.MessageEmbed()
                .setDescription(`Successfully Unlocked: ${message.channel}`)
                .setColor(lunercolour)
            )
        } else if (
            channel.permissionsFor(message.guild.id).has('SEND_MESSAGES') === true
        ) {
            channel.updateOverwrite(message.guild.id, {
                SEND_MESSAGES: false,
            })

            message.channel.send(
                new Discord.MessageEmbed()
                .setDescription(`Successfully Locked: ${message.channel}`)
                .setColor(lunercolour)
            )
        }
    }
}