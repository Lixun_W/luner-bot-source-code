const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Kick someone from the voice channel they are in',
            category: 'Voice Moderation',
            usage: '<@User>'
        });
    }

    async run(message) {

        if (!message.member.hasPermission('MOVE_MEMBERS')) return;
        if (message.member.bot) return;
        if (message.channel.type === 'DM') return;

        const member = message.mentions.members.first()

        if (!member) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Voicekick`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Kick someone from the voice channel they are in`)
                    .addField(`Usage:`, `${this.client.prefix}voicekick <@User>`)
                    .addField(`Example`, `${this.client.prefix}voicekick @Lukas`)
            )
        }

        member.voice.kick()

        message.channel.send(
            new Discord.MessageEmbed()
                .setTitle(`Disconnected`)
                .setColor(lunercolour)
                .setDescription(`<@!${member.id}> Has been kicked from their current voice channel`)
                .setTimestamp()
                .setFooter(`User Kicked`)
        )
    }
}