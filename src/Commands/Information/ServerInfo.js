const { MessageEmbed } = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json')
const moment = require('moment')

const verificationLevels = {
    NONE: 'None',
    LOW: 'Low',
    MEDIUM: 'Medium',
    HIGH: '(╯°□°）╯︵ ┻━┻',
    VERY_HIGH: '┻━┻ ﾐヽ(ಠ益ಠ)ノ彡┻━┻'
};
const filterLevels = {
    DISABLED: 'Off',
    MEMBERS_WITHOUT_ROLES: 'No Role',
    ALL_MEMBERS: 'Everyone'
};
const regions = {
    brazil: 'Brazil',
    europe: 'Europe',
    hongkong: 'Hong Kong',
    india: 'India',
    japan: 'Japan',
    russia: 'Russia',
    singapore: 'Singapore',
    southafrica: 'South Africa',
    sydeny: 'Sydeny',
    'us-central': 'US Central',
    'us-east': 'US East',
    'us-west': 'US West',
    'us-south': 'US South'
};
module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            aliases: ['si', 'server'],
            description: 'Displays the information about the server',
            category: 'Information',
            usage: ''
        });
    }
    async run(message, ...args) {
        message.delete()
        const guild = message;
        const roles = message.guild.roles.cache.sort((a, b) => b.position - a.position).map(role => role.toString());
        const members = message.guild.members.cache;
        const channels = message.guild.channels.cache;
        const emojis = message.guild.emojis.cache;

        const ServerInfoEmbed = new MessageEmbed()
            .setThumbnail(message.guild.iconURL({ dynamic: true }))
            .setAuthor(message.author.username, message.author.displayAvatarURL())
            .setColor(lunercolour)
            .setDescription(`Shows the server info for \`${message.guild.name}\``)
            .addField('General Info', [
                `**<a:arrowthingy:792479964632317992> ID:** ${message.guild.id}`,
                `**<a:arrowthingy:792479964632317992> Name:** ${message.guild.name}`,
                `**<a:arrowthingy:792479964632317992> Owner:** ${message.guild.owner} (${message.guild.owner.id})`,
                `\u200b`
            ])
            .addField('Boost Info', [
                `**<a:arrowthingy:792479964632317992> Boost Tier:** ${message.guild.premiumTier ? `Tier ${message.guild.premiumTier}` : 'None'}`,
                `**<a:arrowthingy:792479964632317992> Boost Count:** ${message.guild.premiumSubscriptionCount || '0'}`,
                `\u200b`
            ])
            .addField('Counters', [
                `**<a:arrowthingy:792479964632317992> Role Count:** ${roles.length}`,
                `**<a:arrowthingy:792479964632317992> Text Channels:** ${channels.filter(channel => channel.type === 'text').size}`,
                `**<a:arrowthingy:792479964632317992> Voice Channels:** ${channels.filter(channel => channel.type === 'voice').size}`,
                `**<a:arrowthingy:792479964632317992> Bots:** ${members.filter(member => member.user.bot).size}`,
                `**<a:arrowthingy:792479964632317992> Humans:** ${members.filter(member => !member.user.bot).size}`,
                `**<a:arrowthingy:792479964632317992> Animated Emoji Count:** ${emojis.filter(emoji => emoji.animated).size}`,
                `**<a:arrowthingy:792479964632317992> Emoji Count:** ${emojis.size}`,
                `**<a:arrowthingy:792479964632317992> Regular Emoji Count:** ${emojis.filter(emoji => !emoji.animated).size}`,
                `\u200b`
            ])
            .addField('Additional Info', [
                `**<a:arrowthingy:792479964632317992> Explicit Filter:** ${filterLevels[message.guild.explicitContentFilter]}`,
                `**<a:arrowthingy:792479964632317992> Verification Level:**  ${verificationLevels[message.guild.verificationLevel]}`,
                `**<a:arrowthingy:792479964632317992> Time Created:** ${moment(message.guild.createdTimestamp).format('LT')} ${moment(message.guild.createdTimestamp).format('LL')} ${moment(message.guild.createdTimestamp).fromNow()}`,
                `**<a:arrowthingy:792479964632317992> Region:** ${regions[message.guild.region]}`,
                `\u200b`
            ])
            .setTimestamp()
            .setFooter(`Requested By: ${message.author.username}`)
        
        message.channel.send(ServerInfoEmbed)
    }
}