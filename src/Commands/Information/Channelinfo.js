const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const args = [1]

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {    
            description: 'Show information on a selected channel',
            category: 'Information',
            aliases: ['ci'],
            usage: '[#Channel]'
        }) 
    }

    async run(message) {
        message.delete()

        const channel = message.mentions.channels.first() || message.channel;


        const ChannelInfoEmbed = new Discord.MessageEmbed()
        .setTitle(`Channel Info`)
        .setColor(lunercolour)
        .setFooter(`Requested by: ${message.author.username}`)
        .setTimestamp()
        .setDescription(`Showing Channel Information On \`${channel.name}\``)
        .addFields({
            name: `<a:arrowthingy:792479964632317992> Channel Name`,
            value: `${channel.name}`,
        },
        {
            name: `<a:arrowthingy:792479964632317992> Channel Id`,
            value: `${channel.id}`,
        },
        {
            name: '<a:arrowthingy:792479964632317992> Channel Type',
            value: `${channel.type}`,
        },
        {
            name: `<a:arrowthingy:792479964632317992> Channel Mention`,
            value: `\`<#${channel.id}>\``
        }
        )

        message.channel.send(ChannelInfoEmbed)
    }
}