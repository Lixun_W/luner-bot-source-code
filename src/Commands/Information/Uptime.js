const { MessageEmbed } = require ("discord.js")
const Command = require ("../../Structures/Command")
const { lunercolour } = require ("../../../config.json")

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Shows the uptime of the bot',
            category: 'Information',
            aliases: ['ut'],
            usage: ''
        }) 
    }

    async run(message) {
        message.delete()
    function duration(ms) {
        const sec = Math.floor((ms / 1000) % 60).toString()
        const min = Math.floor((ms / (1000 * 60)) % 60).toString()
        const hrs = Math.floor((ms / (1000 * 60 * 60)) % 60).toString()
        const day = Math.floor((ms / (1000 * 60 * 60 * 24)) % 24).toString()
        return `${day.padStart(1, '0')} days\n${hrs.padStart(2, '0')} hours\n${min.padStart(2, '0')} minutes\n${sec.padStart(2, '0')} seconds\n`
    }
    const uEmbed = new MessageEmbed()
        .setTitle(`${this.client.user.username} Bot Uptime`)
        .setThumbnail(this.client.user.displayAvatarURL())
        .setAuthor(message.author.username, message.author.displayAvatarURL())
        .setColor(lunercolour)
        .addField(`<a:arrowthingy:792479964632317992> | **Uptime:**`, `${duration(this.client.uptime)}`)
        .setFooter(`${this.client.user.username} | Uptime`, `${this.client.user.displayAvatarURL()}`)
    message.channel.send(uEmbed)
    }
}