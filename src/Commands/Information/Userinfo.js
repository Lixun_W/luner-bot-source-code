const { MessageEmbed, Discord } = require('discord.js');
const { lunercolour } = require('../../../config.json');
const Command = require('../../Structures/Command');
const moment = require('moment')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Show information on a user',
            aliases: ['whois', 'ui', 'user'],
            category: 'Information',
            usage: '[@User]'
        })
    }

    async run(message) {
        message.delete()
        const user = message.mentions.members.last() || message.member;

        const roles = user.roles.cache
        .sort((a, b) => b.position - a.position)
        .map(role => role.toString())
        .slice(0, -1);

        const UserInfoEmbed = new MessageEmbed()
        .setTitle(`User Info`) // this command sucks
        .setColor(lunercolour)
        .setFooter(`Requested by: ${message.author.username}`)
        .setTimestamp()
        .setDescription(`Showing User Information On \`${user.user.username}\``)
        .setThumbnail(user.user.displayAvatarURL({ dynamic: true, size: 512 }))
        .addFields({
            name: `<a:arrowthingy:792479964632317992> Name`,
            value: `${user.user.username}#${user.user.discriminator}`,
        },
        {
            name: `<a:arrowthingy:792479964632317992> Id`,
            value: `${user.user.id}`
        },
        {
            name: `<a:arrowthingy:792479964632317992> Joined Discord`,
            value: `${moment(user.user.createdTimestamp).format('LT')} ${moment(user.user.createdTimestamp).format('LL')} (${moment(user.user.createdTimestamp).fromNow()})`,
        },
        {
            name: `<a:arrowthingy:792479964632317992> Joined Server`,
            value: `${moment(user.joinedAt).format('LL LTS')}`,
        },
        {
            name: `<a:arrowthingy:792479964632317992> Highest Role`,
            value: `${user.roles.highest.id === message.guild.id ? 'None' : user.roles.highest.name}`,
        }
        )
        console.error();

        message.channel.send(UserInfoEmbed)

    }
 }