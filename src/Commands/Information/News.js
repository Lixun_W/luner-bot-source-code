const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Show the recent news on the bot',
            category: 'Information',
        })
    }

    async run(message) {

        message.channel.send(
            new Discord.MessageEmbed()
            .setTitle(`Recent News And Updates For ${this.client.user.tag}`)
            .setColor(lunercolour)
            .setDescription(`Here is the recent news and updates on Luner!`)
            .addField(`V2`, `Luner's Rework is now out and available for all to use. Please note any and all data stored on the database would have been erased due to a switch to MongoDB. We are sorry for any inconviniece`)
            .addField(`Changes`, `With Luner now using an efficient database it now has many capabilities which can be show when running. \`${this.client.prefix}help\` Make sure to check these out!`)
            .addField(`More Updates`, `More updates are soon to come however I have been mainly working on V2 Its self to get it out to everyone! V2 Has a much sleaker interface and lacks the Roleplay Commands!`)
            .addField(`New Developer`, `With V2 I know take alongside **Lachlan T** with him as Assistant Head Developer please make sure to give him a merry welcome!`)
            .setFooter(`Run ${this.client.prefix}invite to invite Luner`)
            .setTimestamp()
        )
    }
}