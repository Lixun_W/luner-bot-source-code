const { MessageEmbed } = require ("discord.js")
const { lunercolour } = require ("../../../config.json")
const Command = require('../../Structures/Command')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            aliases: ['ri', 'role'],
            description: 'Displays the information about the mentioned role',
            category: 'Information',
            usage: '[@Role]'
        });
    }
    async run(message, ...args) {
        message.delete()
        const role = message.mentions.roles.first() || message.guild.roles.cache.get(args[0]) || message.guild.roles.cache.find(r => r.name.toLowerCase() === args.join(' ').toLocaleLowerCase())
        
        if (!role) {
            return message.channel.send("**Please Enter A Valid Role!**")
        }

        const membersWithRole = message.guild.members.cache.filter(member => member.roles.cache.find(r => r.name === role.name))

        const RoleInfoEmbed = new MessageEmbed()
            .setThumbnail(message.guild.iconURL())
            .setColor(role.color)
            .setDescription(`Showing info about \`${role.name}\``)
            .addField('General Info', [
                `**<a:arrowthingy:792479964632317992> ID:** ${role.id}`,
                `**<a:arrowthingy:792479964632317992> Name:** ${role.name}`,
                `**<a:arrowthingy:792479964632317992> Mention:** \`<@&${role.id}>\``,
                `**<a:arrowthingy:792479964632317992> Members:** ${(membersWithRole).size}`,
                `**<a:arrowthingy:792479964632317992> Guild:** ${role.guild}`,
                `\u200b`
                
            ])
            .addField('Options' , [
                `**<a:arrowthingy:792479964632317992> Hex Colour:** ${role.hexColor}`,
                `**<a:arrowthingy:792479964632317992> Hoisted:** ${role.hoist}`,
                `**<a:arrowthingy:792479964632317992> Mentionable:** ${role.mentionable}`,
                `**<a:arrowthingy:792479964632317992> Managed:** ${role.managed}`,
                `**<a:arrowthingy:792479964632317992> Position:** ${role.position}`,
                `\u200b`
            ])
            .setFooter(`Requested By: ${message.author.username}`)
        
        message.channel.send(RoleInfoEmbed)
    }
}