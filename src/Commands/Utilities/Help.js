const { MessageEmbed } = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json')

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['halp'],
			description: 'Displays all the commands in the bot',
			category: 'Utilities',
			usage: ''
		});
	}

	async run(message, command) {

		const member = message.author

		const embed = new MessageEmbed()
			.setColor(lunercolour)
			.setAuthor(`${message.guild.name} Help Menu`, message.guild.iconURL({ dynamic: true }))
			.setThumbnail(message.guild.iconURL({ dynamic: true }))
			.setFooter(`Requested by ${message.author.username}`, message.author.displayAvatarURL({ dynamic: true }))
			.setTimestamp();

		if (command) {
			const cmd = this.client.commands.get(command) || this.client.commands.get(this.client.aliases.get(command));

			if (!cmd) return message.channel.send(`Dumbass this is an invalid command. \`${command}\``);

			embed.setAuthor(`${this.client.utils.capitalise(cmd.name)} Command Help`, this.client.user.displayAvatarURL());
			embed.setTitle(`Command Parameters: \`<>\` is strict & \`[]\` is optional`)
			embed.setDescription([
				`**<a:arrowthingy:792479964632317992> Aliases:** ${cmd.aliases.length ? cmd.aliases.map(alias => `\`${alias}\``).join(' ') : 'No Aliases'}`,
				`**<a:arrowthingy:792479964632317992> Description:** ${cmd.description}`,
				`**<a:arrowthingy:792479964632317992> Category:** ${cmd.category}`,
				`**<a:arrowthingy:792479964632317992> Usage:** \`\`\`css\n${cmd.usage}\`\`\``
			]);

			return message.channel.send(embed);
		} else {
			embed.setTitle('Support Server!')
			embed.setDescription([
				`These are the available commands for ${message.guild.name}`,
				`The bot's prefix is: \`${this.client.prefix}\``,
				`Command Parameters: \`<>\` is strict & \`[]\` is optional`

			]);
			embed.setURL('https://discord.gg/e5a4FPkmMm')
			let categories;
			categories = this.client.utils.removeDuplicates(this.client.commands.map(cmd => cmd.category));

			for (const category of categories) {
				embed.addField(`<a:arrowthingy:792479964632317992> **${this.client.utils.capitalise(category)}**`, this.client.commands.filter(cmd =>
					cmd.category === category).map(cmd => `\`${cmd.name}\``).join(' | '));
			}
			message.channel.send(embed)
		}

	}
}