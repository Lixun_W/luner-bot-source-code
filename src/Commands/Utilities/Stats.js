const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');
const { version } = require('../../../package.json');
const ms = require('ms');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Show stats on Luner',
            category: 'Utilities',
        })
    }

    async run(message) {

        function duration(ms) {
            const sec = Math.floor((ms / 1000) % 60).toString()
            const min = Math.floor((ms / (1000 * 60)) % 60).toString()
            const hrs = Math.floor((ms / (1000 * 60 * 60)) % 60).toString()
            const day = Math.floor((ms / (1000 * 60 * 60 * 24)) % 24).toString()
            return `${day.padStart(1, '0')} days ${hrs.padStart(2, '0')} hours\n${min.padStart(2, '0')} minutes ${sec.padStart(2, '0')} seconds`
        }

        const StatEmbed = new Discord.MessageEmbed()
        .setTitle(`Stats on ${this.client.user.tag}`)
        .setColor(lunercolour)
        .setTimestamp()
        .setFooter(`Requested By: ${message.author.username}`)
        .addField(`Version:`, `${version}`, true)
        .addField(`Ping:`, `${Math.round(this.client.ws.ping)}ms`, true)
        .addField(`Uptime:`, `${duration(this.client.uptime)}`, true)
        .addField(`Command Count:`, `${this.client.commands.size}`, true)
        .addField(`Server Count:`, `${this.client.guilds.cache.size}`, true)
        .addField(`User Count:`, `${this.client.guilds.cache.reduce((a, b) => a + b.memberCount, 0)}`, true)

        message.channel.send(StatEmbed)
    }
}