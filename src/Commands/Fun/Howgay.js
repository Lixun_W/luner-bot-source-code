const Discord = require('discord.js')
const Command = require('../../Structures/Command')

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            aliases:  '',
            description: 'Let Luner decide how gay you are today.',
            category: 'Fun',
            usage: ''
        })
    }


async run(message) {

    const member = message.mentions.users.first() || message.author
const love = Math.random() * 100;
const loveIndex = Math.floor(love / 10);
const loveLevel = "🎁".repeat(loveIndex) + "📄".repeat(10 - loveIndex);

const embed = new Discord.MessageEmbed()
    .setColor('BLUE')
    .setAuthor(`${message.author.username}`, `${message.author.displayAvatarURL()}`)
    .addField(`☁ **${member.username}** is **${Math.floor(love)}% gay Today!**`,`\n\n${loveLevel}`);

message.channel.send(embed);
}
}