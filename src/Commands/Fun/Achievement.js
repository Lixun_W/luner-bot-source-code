const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Give someone an achievement',
            category: 'Fun',
            usage: '<Achievement>'
        })
    }

    async run(message) {

        const achievement = message.content.substring(12)

        if (!achievement) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Achievement`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Give someone an achievement`)
                    .addField(`Usage:`, `${this.client.prefix}achievement <Achievement>`)
                    .addField(`Example`, `${this.client.prefix}achievement well done!`)
            )
        }

        message.channel.send(
            new Discord.MessageEmbed()
            .setTitle(`New Achievement!`)
            .setColor(lunercolour)
            .setDescription(`A new achievement has been handed out: **${achievement}**`)
            .setTimestamp()
            .setFooter(`Given by: ${message.author.username}`)
        )
    }
}