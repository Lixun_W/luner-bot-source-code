const Discord = require('discord.js');
const Command = require('../../Structures/Command');
const figlet = require('util').promisify(require('figlet'));
const { lunercolour } = require('../../../config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            description: 'Put something into a code block',
            category: 'Fun',
            usage: '<Text>',
            aliases: ['banner']
        })
    }

    async run(message, ...banner) {

        const bannar = message.content.substring(7)

        if (!bannar) {
            return message.channel.send(
                new Discord.MessageEmbed()
                    .setTitle(`Command: Bannar`)
                    .setColor(lunercolour)
                    .addField(`Description:`, `Put something into a code block`)
                    .addField(`Usage:`, `${this.client.prefix}bannar <Text>`)
                    .addField(`Example`, `${this.client.prefix}bannar Hey!`)
            )
        }

        message.channel.send(await figlet(banner), { code: true });
    }
}