const Command = require('../../Structures/Command');
const Discord = require('discord.js');

module.exports = class extends Command {

    constructor(...args) {
        super(...args,  {
            alises: ['Random-Hex'],
            description: 'Use this command to generate a random hex code',
            category: 'Fun',
            usage: ''
        })
    }

    async run(message) {

        var randomColor = Math.floor(Math.random()*16777215).toString(16);

        const randomColour = new Discord.MessageEmbed()
        .setTitle('Random Hex Colour')
        .setColor(randomColor)
        .setDescription(`#${randomColor}`)
        .setTimestamp()

        message.channel.send(randomColour)
    }
}