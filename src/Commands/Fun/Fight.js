const Discord = require("discord.js")
const Command = require("../../Structures/Command");
let beasthealth = 150;
let userhealth = 100;

module.exports = class extends Command {
    constructor(...args) {
		super(...args, {
			aliases: ['beast', 'fightit'],
			description: 'Fight the discord beast! Tip: run the command every single time until you die or you win.',
      category: 'Fun',
      Usage: ''
		});
    }
    
    async run(message, args) {
        let beastdamage = Math.floor(Math.random() * 5);
        let userdamage = Math.floor(Math.random() * 30);
        //message.delete();
        userhealth = userhealth <= 0 ? 100 : userhealth - beastdamage;
        beasthealth = beasthealth <= 0 ? 100 : beasthealth - userdamage;
        
        
        let Embed1 = new Discord.MessageEmbed()
        .setColor("#66ccff")
        .setDescription("➡️ You dealt **" + userdamage + "** damage! And the discord beast has **" + beasthealth + "** health left...");
        
        
        let Embed2 = new Discord.MessageEmbed()
        .setColor("#66ccff")
        .setDescription(`🏆 You dealt **${userdamage}** damage! The discord beast has died! You saved **${message.guild.name}**!`);
        
        
        let Embed3 = new Discord.MessageEmbed()
        .setColor("#ff3300")
        .setDescription("⬅️ The discord beast dealt **" + beastdamage + "** damage! And you have **" + userhealth + "** health left...")
        
        
        let Embed4 = new Discord.MessageEmbed()
        .setColor("#ff3300")
        .setDescription(`🐲 The discord beast dealt **${beastdamage}** damage! You have died! The discord beast destroyed **${message.guild.name}**!`)
        
        if (beasthealth <= 0)
          return message.channel.send(Embed2);
        message.channel.send(Embed1).then(console.log(`➡️ Dealt ${userdamage} damage to the discord beast in ${message.guild.name}! The discord beast now has ${beasthealth} health left!`));
        if (userhealth <= 0)
          return message.channel.send(Embed4).then((userhealth <= 0)((userhealth = userhealth)));
        message.channel.send(Embed3).then(console.log(`⬅️ The discord beast dealt ${beastdamage} damage to the user! In ${message.guild.name}! The user now has ${userhealth} health left!`));
      }
    };