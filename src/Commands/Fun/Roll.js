const Command = require('../../Structures/Command');
const Discord = require('discord.js')

const answers = [
    '9',
    '8',
    '7',
    '6',
    '5',
    '4',
    '3',
    '2',
    '1'
];

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            aliases: '',
            description: 'Let the dice roll you a random number',
            category: 'Fun',
            usage: ''
        })
    }

    async run(message) {
        const dice = 'https://gilkalai.files.wordpress.com/2017/09/dice.png'
        const roll = new Discord.MessageEmbed()
        .setColor('BLUE')
        .setAuthor(`${message.author.username}`, `${message.author.displayAvatarURL()}`)
        .setTitle('You rolled A....?')
        .setThumbnail(dice)
        .setDescription(`${answers[Math.floor(Math.random() * answers.length)]}`)
		message.channel.send(roll)
	}    
}