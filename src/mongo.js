const mongoose = require('mongoose')
const mongoPath = 'YOUR MONGO PATH'

module.exports = async () => {
  await mongoose.connect(mongoPath, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    connectTimeoutMS: 30000,
    keepAlive: 1,
  })
  return mongoose
}
