const mongoose = require('mongoose')

const unbanSchema = mongoose.Schema({
    guildId: {
        type: String,
        required: true,
    },
    userId: {
        type: String,
        required: true,
    },
    unbans: {
        type: [Object],
        required: true,
    },

})

module.exports = mongoose.model('unbans', unbanSchema)