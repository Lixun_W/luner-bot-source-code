const mongoose = require('mongoose')

const modlogSchema = mongoose.Schema({
    guildId: {
        type: String,
        required: true,
      },
      type: {
          type: String,
          required: true,
      },
      modlogs: {
          type: [Object],
          required: true,
      }
})

module.exports = mongoose.model('modlog', modlogSchema)